This is a Helm chart designed to deploy the widest set of apps in kubernetes using muppy.io.

This is called meta package because what is deploys is configured in 'mpy_parts_config-values.yaml'.

mpy.metapackage is code named m2p. You will find this name in templates.

m2p est concu pour etre utilisé avec muppy mais est utilisable avec helm.

# History

v2.x ; values.mpy_parts_config is renamed values.mpy_meta_config since it now allows to define more than parts (eg. volumes)


# Reste a faire

 - Faire un values neutre compatible avec la surcharge
 - créer la part imq_workers
 - ajouter un flag 'enabled' dans les parts
 - permettre de définir un docker / registry par part
 - ajouter une  option permettant de gérer un config file

This packages a set of parts.
Each part may a Deployment or a Web service with toutes.
Packages also supports deployment of CronJobs and Jobs


# Use as a Chart

Being a 'meta' chart, m2p does nothing by itself.
You must configure a set of parts to deploy.
For that you must supply a `mpy_parts_config-values.yaml` file which describe a set 
of parts and how to deploy them.

To use m2p as a chart, you must supply




# Use as subchart

Being 'meta', m2p is often used as dependency.






# Usage

```bash
# In package folder
cp values-template.yaml values.yaml
# In values, edit acme_registration_email and package_gui_fqdn

helm install --debug --dry-run --create-namespace --namespace m2ptest httpbin .  -f values.yaml -f mpy_meta_config-values.yaml  >  helm-debug.txt
helm install --create-namespace --namespace m2ptest httpbin . -f values.yaml -f mpy_parts_config-values.yaml 
helm install -n m2ptest httpbin . -f values.yaml -f mpy_parts_config-values.yaml 


helm --namespace m2ptest delete httpbin 
helm -n m2ptest  delete httpbin

helm install --debug --dry-run --namespace toctoc toctoc ../m2p -f toctoc-4a6fdd06-mpy_values.yaml > helm-debug.txt
helm install --debug --dry-run --namespace toctoc toctoc . -f values-app_config.yaml -f values.yaml 

helm install --namespace toctoc toctoc ../m2p -f toctoc-4a6fdd06-mpy_values.yaml 
helm delete -n toctoc toctoc

helm install --namespace oursbleu my-release ./m2p -f values.yaml  -f values-app_config.yaml 
```

# Package this chart 

Change to the chart folder then use:

## To package the chart

 - Adjust content
 - Update version
 - commit changes

You can generate a package archive with:
```bash
# cd to the chart repo
helm package ../mpy-metapackage --destination ../pkg
```


## To publish the package to Gitlab Registry

Adjust and commit all changes before.

```bash
cd utils
export HELM_CHART_NAME=$(basename $(helm package --dependency-update .. | awk '{print $NF}'))  && echo "chart name=${HELM_CHART_NAME}" 
export GITLAB_REGISTRY_USERNAME=cmorisse
export GITLAB_REGISTRY_TOKEN=$REPO_GIT_TOKEN
export GITLAB_PROJECT_ID=56199255
# To upload the package to gitlab registry
curl --request POST \
    --form "chart=@$HELM_CHART_NAME" \
    --user $GITLAB_REGISTRY_USERNAME:$GITLAB_REGISTRY_TOKEN \
    https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/packages/helm/api/stable/charts
rm $HELM_CHART_NAME
```

# To install the package from Gitlab Package Repository

```
helm repo add mpy-metapackage-charts https://gitlab.com/api/v4/projects/56199255/packages/helm/stable
helm repo update mpy-metapackage-charts

helm install one mpy-metapackage-charts/mpy-metapackage
```
# Notes

Docker image can be specified (by order of ascending prioritiy):
 
 - In Profile ; reinjected into values.docker_image
 - In values.docker_image (via overload)
 - Directly in mpy_meta_config.parts.{{name}}.deployment.docker_image


# Dashboard management

Basic parts (with no debug mode) do not require dashboard. 

For now, parts with dashboard required user to create them manually. See example.

Target
Muppy will generate dashboard from data in mpy_meta_config.parts.
Values will contain template one instruction to reinject dashboard content in values:
```
dashboards:
  @{ obj.get_dashboards_json() | to_yaml(offset=2) }@
```

# Changelog

  - 2.3.12 - add support for cronjobs
  - 2.3.11 - add support for volumes and securityContext
  - 2.3.10 - adds startup probes