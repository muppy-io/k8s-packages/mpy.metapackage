
# .Chart.Name = mpy.metapackage-python
# .Release.Name = instance name as entered in muppy

{{- define "m2p.key" -}}
{{- .Values.package_release.key  }}
{{- end }}

{{- define "m2p.fullname" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
# Common labels
# https://helm.sh/docs/chart_best_practices/labels/

#This should be the app name, reflecting the entire app. 
#Usually {{ template "name" . }} is used for this. 
#This is used by many Kubernetes manifests, and is not Helm-specific.
#In m2p we use the Profile App Name
app.kubernetes.io/name: {{ .Values.package_release.app_name }}

# The version of the app and can be set to {{ .Chart.AppVersion }}.
# In m2p we use the Profile App Version
app.kubernetes.io/version: {{ .Values.package_release.app_version }}

# This should be the chart name and version: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}.
helm.sh/chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}

# This should always be set to {{ .Release.Service }}. 
# It is for finding all things managed by Helm.
app.kubernetes.io/managed-by: {{ .Release.Service }}

# This should be the {{ .Release.Name }}. It aids in differentiating 
# between different instances of the same application.
app.kubernetes.io/instance: {{ .Release.Name }}

# This is used by muppy to identify package's objects.
muppy.io/package-release: {{ .Values.package_release.name }}

*/}}
# Note that Component should be manually set in templates
#app.kubernetes.io/component: "gui"  

{{- define "m2p.labels" -}}
app.kubernetes.io/name: {{ .Values.package_release.app_name }}
app.kubernetes.io/version: "{{ .Values.package_release.app_version }}"
helm.sh/chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
app.kubernetes.io/instance: {{ .Release.Name }}
muppy.io/package-release: {{ .Values.package_release.name }}
{{- end }}

{{- define "m2p.selectorLabels" -}}
app.kubernetes.io/name: {{ .Values.package_release.app_name }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}
