{{- range $part_name, $part_info := .Values.mpy_meta_config.parts }}
{{- $dashboard := dict }}
{{- if hasKey ($.Values.dashboards | default dict) $part_name }}
{{-     $dashboard = index $.Values.dashboards $part_name }}
{{- end }}
{{- $cronjob := (index $.Values.mpy_meta_config.parts $part_name).cronjob }}
{{- $debug_config := (index $.Values.mpy_meta_config.parts $part_name).debug_config }}
{{- if $cronjob }}
kind: CronJob
apiVersion: batch/v1
metadata:
  name: "{{ $.Values.package_release.key }}-{{ $part_name }}"
  namespace: "{{ $.Release.Namespace }}"
  labels:
    app.kubernetes.io/component: "{{ $part_name }}" 
    {{- include "m2p.labels" $ | nindent 4 }}
{{- if $part_info.dashboard }}
  annotations:
    muppy.io/dashboard-name: "{{ $part_info.dashboard.name | default $part_name}}"
    muppy.io/debug-mode-available: {{ $part_info.debug_mode_available }}
{{- end }}
spec:
  {{- if and $dashboard (hasKey $dashboard "suspend") }}
  suspend: {{ $dashboard.suspend }} # defined from dashboard
  {{- else if hasKey $cronjob "suspend" }}
  suspend: {{ $cronjob.suspend }}   # defined from cronjob (no dashboard)
  {{- else }}
  suspend: false   # default value from chart (nothing from dashboard nor from cronjob )
  {{- end }}
{{- if and $debug_config $dashboard.debug_mode }}
  schedule: "* * 31 2 *"  # shedule forced in package by debug_mode
{{- else }}
  {{- if and $dashboard (hasKey $dashboard "schedule") }}
  schedule: "{{ $dashboard.schedule }}" # defined from dashboard
  {{- else if hasKey $cronjob "schedule" }}
  schedule: "{{ $cronjob.schedule }}"   # defined from cronjob
  {{- else }}
  schedule: "* * 31 2 *"  # default value from chart (nothing from dashboard nor from cronjob )
  {{- end }}
{{- end }}
  concurrencyPolicy: {{  $part_info.concurrencyPolicy | default "Forbid" }}
  successfulJobsHistoryLimit: {{  $part_info.successfulJobsHistoryLimit | default 2 }}
  failedJobsHistoryLimit: {{  $part_info.failedJobsHistoryLimit | default 5 }}
  jobTemplate:
    metadata:
      labels:
        {{- include "m2p.labels" $ | nindent 8 }}
        app.kubernetes.io/component: "{{ $part_name }}" 
    spec:
      template:
        metadata:
          labels:
            {{- include "m2p.labels" $ | nindent 12 }}
            app.kubernetes.io/component: "{{ $part_name }}" 
        spec:
          restartPolicy: {{ $part_info.restartPolicy | default "OnFailure" }}
          {{- if $.Values.image_registry_secret }}
          imagePullSecrets:
          - name: "{{ $.Values.package_release.key }}-docker-registry-cred"
          {{- end }}
        {{- if $part_info.volumes }}
          #
          # Volumes
          #
          volumes:
          {{- range $pvc_idx, $pvc_info := $part_info.volumes }}
          - name: {{ $pvc_info.name }}
            persistentVolumeClaim:
              claimName: "{{ $.Values.package_release.key }}-pvc-{{ $pvc_info.name }}"
          {{- end }}{{/* range part_info.volumes */}}
        {{- end }}{{/* if $part_info.volumes */}}
        {{- if $part_info.securityContext }}
          securityContext: {{ $part_info.securityContext | toYaml | nindent 8 }}
        {{- end }} {{/* securityContext */}}
          containers:
          - name: "{{ $.Values.package_release.name }}-{{ $part_name }}"
            image: {{ $cronjob.docker_image | default $.Values.docker_image }}
            imagePullPolicy: {{ $.Values.image_pull_policy | default "Always" }}
        {{- if and (and $debug_config $dashboard.debug_mode) $debug_config.resources }}
            # from dashboard (debug mode)
            resources: {{ $debug_config.resources | toYaml | nindent 14 }}
        {{- else }}
          {{- if and $dashboard $dashboard.resources }}
            # from dashboard
            resources: {{ $dashboard.resources | toYaml | nindent 14 }}
          {{- else if hasKey $cronjob "resources" }}
            # from cronjob definition
            resources: {{ $cronjob.resources | toYaml | nindent 14 }}
          {{- end }}
        {{- end }}{{/*  else $dashboard.debug_mode */}}
        {{- if $part_info.startupProbe }}
            #
            # Probes - cronjob supports only statupProbe (and no Http)
            #
            startupProbe: {{ $part_info.startupProbe | toYaml | nindent 12 }}
        {{- end }} {{/* startupProbe */}}
        {{- if $part_info.volumes }}
            #
            # volumes mounts
            #
            volumeMounts:
          {{- range $vol_idx, $vol_info := $part_info.volumes }}
            - name: {{ $vol_info.name }}
              mountPath: {{ $vol_info.mountPath | required "Missign required mountPath" }}
              readOnly: {{ $vol_info.readOnly | default false }}
          {{- end }}{{/* range part_info.volumes */}}
        {{- end }}{{/* if $part_info.volumes */}}
            #
            # ENV Vars
            # PGDATABASE is always injected when defined
            #
            env:
            {{- if $.Values.common_env_vars  }}
            {{- $.Values.common_env_vars | toYaml | nindent 12 }}
            {{- end }}
            {{- if $cronjob.env_vars  }}
            {{- $cronjob.env_vars | toYaml | nindent 12 }}
            {{- end }}
            {{- if and $dashboard.debug_mode $cronjob.debug_env_vars  }}
            {{- $cronjob.debug_env_vars | toYaml | nindent 12 }}
            {{- end }}
            {{- if and $dashboard.debug_mode $.Values.debug_env_vars  }}
            {{- $.Values.debug_env_vars | toYaml | nindent 12 }}
            {{- end }}
            {{- if $.Values.secrets_env_file }}
            envFrom:
            - secretRef:
                name: "{{ $.Values.package_release.key }}-secrets-envfile"
            {{- end }}
            #
            # Command and args
            #
        {{- if $dashboard.debug_mode }}
          {{- if $debug_config.command }}
            command: 
            {{- $debug_config.command | toYaml | nindent 12 }}
          {{- end }} {{/* $cronjob.debug_command) */}}
          {{- if $debug_config.args }}
            args: 
            {{- $debug_config.args | toYaml | nindent 12 }}
          {{- end }} {{/* $cronjob.debug_args) */}}
        {{- else }} {{/* if $dashboard.debug_mode) */}}
          {{- if $dashboard.command }}
            command:  # defined in dashboard
            {{- $dashboard.command | toYaml | nindent 12 }}
          {{- else }}
            {{- if $cronjob.command }}
            command: # defined in cronjob
            {{- $cronjob.command | toYaml | nindent 12 }}
            {{- else }}
              {{- if not $dashboard }}
              {{ fail "No 'command' defined in 'cronjob' and no dashboard is setup" }}
              {{- else }} 
              {{ fail "No 'command' defined neither in 'cronjob' nor in dashboard." }}
              {{- end }}
            {{- end }}
          {{- end }} {{/* $cronjob.command */}}
          {{- if $dashboard.args }}
            args:    # defined in dashboard
            {{- $dashboard.args | toYaml | nindent 12 }}
          {{- else }}
            {{- if $cronjob.args }}
            args:    # defined in cronjob
            {{- $cronjob.args | toYaml | nindent 12 }}
            {{- else }}
              {{- if not $dashboard }}
              {{ fail "No 'args' defined in 'cronjob' and no dashboard is setup" }}
              {{- else }} 
              {{ fail "No 'args' defined neither in 'cronjob' nor in dashboard." }}
              {{- end }}
            {{- end }}
          {{- end }} {{/* $dashboard.args */}}
        {{- end }} {{/* if $dashboard.debug_mode */}}
{{/* Whaaaou */}}
---
{{ end }}{{/* if cronjob */}}
{{ end }}{{/* .Values.mpy_meta_config.parts */}}

