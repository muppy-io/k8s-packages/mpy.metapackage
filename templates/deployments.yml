{{- range $part_name, $part_info := .Values.mpy_meta_config.parts }}
{{- $dashboard := dict }}
{{- if hasKey ($.Values.dashboards | default dict) $part_name  }}
{{- $dashboard = index $.Values.dashboards $part_name }}
{{- end }}
{{- $deployment := (index $.Values.mpy_meta_config.parts $part_name).deployment }}
{{- $debug_config := (index $.Values.mpy_meta_config.parts $part_name).debug_config }}
{{- if $deployment }}
kind: Deployment
apiVersion: apps/v1
metadata:
  name: "{{ $.Values.package_release.key }}-{{ $part_name }}-deploy"
  namespace: "{{ $.Release.Namespace }}"
  labels:
    {{- include "m2p.labels" $ | nindent 4 }}
    app.kubernetes.io/component: "{{ $part_name }}" 
{{- if $part_info.dashboard }}
  annotations:
    muppy.io/dashboard-name: "{{ $part_info.dashboard.name | default $part_name}}"
    muppy.io/debug-mode-available: "{{ $part_info.debug_mode_available }}"
{{- end }}
spec:
{{- /* Replicas */}}
{{- if and $dashboard (hasKey $dashboard "replica_count") }}
  replicas: {{ $dashboard.replica_count }}  # defined in dashboard
{{- else if $deployment.replica_count }}
  replicas: {{ $deployment.replica_count }}  # from mpy_meta_config since no dashboard id defined
{{- else }}
  {{- fail "'mpy_meta_config.parts...replica_count' must be defined in mpy_meta_config 'part.deployment' or in dashboard." }}
{{- end }}
  selector:
    matchLabels:
      {{- include "m2p.selectorLabels" $ | nindent 6 }}
      app.kubernetes.io/component: "{{ $part_name }}" 
  template:
    metadata:
      labels:
        {{- include "m2p.labels" $ | nindent 8 }}
        app.kubernetes.io/component: "{{ $part_name }}" 
    spec:
{{- if $.Values.image_registry_secret }}
      #
      # imagePullSecrets
      imagePullSecrets:
      - name: "{{ $.Values.package_release.key }}-docker-registry-cred"
{{- end }}
      #
      # Volumes
{{- if $part_info.volumes }}
      volumes:
{{- range $pvc_idx, $pvc_info := $part_info.volumes }}
      - name: {{ $pvc_info.name }}
        persistentVolumeClaim:
          claimName: "{{ $.Values.package_release.key }}-pvc-{{ $pvc_info.name }}"
{{- end }}{{/* range part_info.volumes */}}
{{- end }}{{/* if $part_info.volumes */}}
      #
      # securityContext
{{- if $part_info.securityContext }}
      securityContext: {{ $part_info.securityContext | toYaml | nindent 8 }}
{{- end }} {{/* securityContext */}}
      #
      # containers
      containers:
      - name: "{{ $.Values.package_release.name }}-{{ $part_name }}"
        image: {{ $deployment.docker_image | default $.Values.docker_image }}
        imagePullPolicy: {{ $.Values.image_pull_policy | default "Always" }}
{{- if and (and $debug_config $dashboard.debug_mode) $debug_config.resources }}
        resources: {{ $debug_config.resources | toYaml | nindent 10 }}
{{- else -}}{{/*  if $dashboard.debug_mode  and debug_config.resources */}}
    {{- if and $dashboard $dashboard.resources }}
        resources: {{ $dashboard.resources | toYaml | nindent 10 }}
    {{- else }}
        resources: {{ $deployment.resources | toYaml | nindent 10 }}
    {{- end }}
{{- end }}{{/*  else $dashboard.debug_mode */}}
        #
        # ports
        ports:
        {{- range $port_info := $part_info.ports }}
         - containerPort: {{ $port_info.port }}
        {{- end }}{{/* range $port_info */}}
        #
        # Probes
{{- if $part_info.readinessProbe }}
        readinessProbe: {{ $part_info.readinessProbe | toYaml | nindent 10 }}
{{- end }} {{/* readinessProbe */}}
{{- if $part_info.livenessProbe }}
        livenessProbe: {{ $part_info.livenessProbe | toYaml | nindent 10 }}
{{- end }} {{/* livenessProbe */}}
{{- if $part_info.startupProbe }}
        startupProbe: {{ $part_info.startupProbe | toYaml | nindent 10 }}
{{- end }} {{/* startupProbe */}}
        #
        # volumes mounts
{{- if $part_info.volumes }}
        volumeMounts:
{{- range $vol_idx, $vol_info := $part_info.volumes }}
        - name: {{ $vol_info.name }}
          mountPath: {{ $vol_info.mountPath | required "Missign required mountPath" }}
          readOnly: {{ $vol_info.readOnly | default false }}
{{- end }}{{/* range part_info.volumes */}}
{{- end }}{{/* if $part_info.volumes */}}
        #
        # ENV Vars
        # PGDATABASE is always injected when defined
        env:
        {{- if $.Values.common_env_vars  }}
        {{- $.Values.common_env_vars | toYaml | nindent 8 }}
        {{- end }}
        {{- if $deployment.env_vars  }}
        {{- $deployment.env_vars | toYaml | nindent 8 }}
        {{- end }}
        {{- if and $dashboard.debug_mode $deployment.debug_env_vars  }}
        {{- $deployment.debug_env_vars | toYaml | nindent 8 }}
        {{- end }}
        {{- if and $dashboard.debug_mode $.Values.debug_env_vars  }}
        {{- $.Values.debug_env_vars | toYaml | nindent 8 }}
        {{- end }}
        {{- if $.Values.secrets_env_file }}
        envFrom:
        - secretRef:
            name: "{{ $.Values.package_release.key }}-secrets-envfile"
        {{- end }}
        #
        # Command and args. 
    {{- if $dashboard.debug_mode }}
      {{- if $debug_config.command }}
        command: 
        {{- $debug_config.command | toYaml | nindent 8 }}
      {{- end }} {{/* $deployment.debug_command) */}}
      {{- if $debug_config.args }}
        args: 
        {{- $debug_config.args | toYaml | nindent 8 }}
      {{- end }} {{/* $deployment.debug_args) */}}
    {{- else }} {{/* not $dashboard.debug_mode) */}}
      {{- if $deployment.command }}
        command: 
        {{- $deployment.command | toYaml | nindent 8 }}
      {{- end }} {{/* $deployment.command */}}
      {{- if $deployment.args }}
        args: 
        {{- $deployment.args | toYaml | nindent 8 }}
      {{- end }} {{/* $deployment.command */}}
    {{- end }} {{/* $dashboard.debug_mode */}}
{{/* Whaaaou */}}
---
{{ end }}
{{ end }}

